// { posts } ini ngambil dari hasil fungsi getServerSideProps
export default function PakeServerSideRendering({ posts }) {
  return (
    <div className="container">
      {posts.map((row, i) => (
        <div className="card" key={i}>
          <h2>{row.title}</h2>
          <p>{row.body}</p>
        </div>
      ))}
    </div>
  );
}

export async function getServerSideProps() {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts");
  const posts = await response.json();

  return {
    props: {
      posts,
    },
  };
}
