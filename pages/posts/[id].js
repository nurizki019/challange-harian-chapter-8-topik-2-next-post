export default function Coba({ title, body }) {
  return (
    <div className="card ">
      <h2>{title}</h2>
      <p>{body}</p>
    </div>
  );
}
export async function getStaticPaths() {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts");
  const post = await response.json();

  const paths = post.map(({ id }) => ({
    params: {
      id: id.toString(),
    },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts/" + params.id);
  const post = await response.json();

  return {
    props: {
      ...post,
    },
  };
}
